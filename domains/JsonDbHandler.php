<?php
namespace app\domains;

class JsonDbHandler extends \yii\base\BaseObject
{
    /**
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public static function findOne(array $params)
    {
        $dbData = static::getFile();
        $keys = array_keys($params);
        $key = $keys[0];

        foreach ($dbData as $row)
        {
            if($row[$key] == $params[$key])
            {
                return $row;
            }
        }

        return null;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public static function getFile()
    {
        $fileName = \Yii::$app->params['fileDb'];
        $filePath = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $fileName;

        if(!file_exists($filePath))
        {
            $createFile = fopen($filePath,"w");
            fwrite($createFile,"");
            fclose($createFile);
        }

        $fileContent = file_get_contents($filePath);

        if (is_string($fileContent))
        {
            return \yii\helpers\Json::decode($fileContent);
        }

        throw new \Exception('Nem sikeült a fájl olvasni.');
    }

    /**
     * @param array $dbFile
     * @return bool
     * @throws \Exception
     */
    public static function saveFile(array $dbFile)
    {
        $fileName = \Yii::$app->params['fileDb'];
        $filePath = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $fileName;

        $saveFile = file_put_contents($filePath, \yii\helpers\Json::encode($dbFile));

        if(!$saveFile){
            throw new \Exception('Nem sikeült a fájl menteni.');
        }

        return true;
    }
}