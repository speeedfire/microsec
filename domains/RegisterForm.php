<?php


namespace app\domains;
use app\domains\ar\Users;
use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
    public $email;
    public $password;
    public $username;
    public $birthday;
    private $hash;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password', 'username', 'birthday'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => '\app\domains\ar\Users'],
            [['username', 'email', 'password'], 'string', 'max' => 64]
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function register()
    {
        return $this->createNewUser();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function createNewUser()
    {
        $user = new Users();
        $user->load($this->getAttributesWithData('model'));

        $dbSave = $user->save();

        if(!$dbSave)
        {
            throw new \Exception('Nem sikerült menteni az adatokat');
        }

        $jsonDb = new JsonDbHandler();
        $dbFile = $jsonDb::getFile();
        $dbFile = is_array($dbFile) ? $dbFile : [];

        array_push($dbFile, array_merge($this->getAttributesWithData('file'), ['idusers' => $user->idusers]));

        return $jsonDb::saveFile($dbFile);
    }

    /**
     * @param $type
     * @return array|bool
     */
    private function getAttributesWithData($type)
    {
        $attributes = [
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->getHash(),
            'birthday' => $this->birthday
        ];

        switch ($type) {
            case 'file':
                return $attributes;
            case 'model':
                return ['Users' => $attributes];
            default:
                return false;
        }
    }

    /**
     * @return mixed
     * @throws \yii\base\Exception
     */
    private function getHash()
    {
        if($this->hash == false)
        {
            $this->setHash();
        }

        return $this->hash;
    }

    /**
     * @throws \yii\base\Exception
     */
    private function sethash()
    {
        $this->hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
    }
}