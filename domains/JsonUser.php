<?php


namespace app\domains;

use yii\web\IdentityInterface;

class JsonUser extends JsonDbHandler implements \yii\web\IdentityInterface
{
    public $username;
    public $password;
    public $birthday;
    public $email;
    public $idusers;
    public $authKey;
    public $accessToken;

    private $users;

    /**
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public static function findOne(array $params)
    {
        $dbData = JsonDbHandler::getFile();
        $keys = array_keys($params);
        $key = $keys[0];

        foreach ($dbData as $k => $row)
        {
            if($row[$key] == $params[$key])
            {
                return new static($row);
            }
        }

        return null;
    }


    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        $user = JsonDbHandler::findOne(['idusers' => $id]);
        return $user ? new static($user) : null;
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = JsonDbHandler::findOne(['accestoken' => $token]);
        return $user ? new static($user) : null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }


    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->idusers;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}