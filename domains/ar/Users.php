<?php

namespace app\domains\ar;

use app\domains\JsonUser;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $idusers
 * @property string|null $password
 * @property string|null $username
 * @property string|null $birthday
 * * @property string|null $email
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const USERS_SQL_DB = 0;
    const USERS_NO_DB = 1;

    public $authKey;
    public $accessToken;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'username', 'birthday'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['username', 'email', 'password'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusers' => 'Idusers',
            'password' => 'Password',
            'username' => 'Username',
            'email' => 'Email',
            'birthday' => 'Birth Login',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne(['idusers' => $id]);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->idusers;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @param string $hash saved pass
     * @return bool if password provided is valid for current user
     */
    public static function validatePassword($password, $hash)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $hash);
    }

    /**
     * @param mixed $params
     * @param mixed $dbType
     * @return bool|\yii\db\ActiveRecord|null
     * @throws \Exception
     */
    public static function findOne($params, $dbType = null) {
        $type = $dbType !== null ? $dbType : rand(0, 1);

        switch ($type) {
            case static::USERS_SQL_DB:
                return parent::findOne($params);;
            case static::USERS_NO_DB:
                return JsonUser::findOne($params);
            default:
                return null;
        }
    }
}
