Az alkalmazás docker alól fut, így ez szükséges hozzá.

~~~~
Le kell húzni a repót, minden változás a master-be benne van.
~~~~

`git checkout master`

Majd futtatni kell a docker container-t.

`docker-compose up -d`

Ezután be kell lépni a container-be, futtatni a composer update-et, és a migrációkat.

Linux alatt:

`docker exec -it microsec_php_1 bash`

Windows alatt:

`winpty docker exec -it microsec_php_1 bash`

A container alatt pedig ezeket:

`composer update`

`yii migrate up`

`itt a yes-re kell majd menni`

Ezek után böngészőből el lehet érni az alkalmazást már.

http://localhost:8000/index.php?r=user

PHPMyAdmin

user/pass: microsec/Abc123

http://localhost:9191/

Tesztek futtatása docker containerben:

`vendor/bin/codecept run`

A tesztek miatt van egy default demo felhasználó a rendszerhez.

Emial/pass: demo@demo.hu/demo

Bármi kérdés van: info@szabolcs-toth.com