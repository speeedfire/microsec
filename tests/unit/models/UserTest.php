<?php

namespace tests\unit\models;

use app\domains\ar\Users;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = Users::findIdentity(1));
        expect($user->username)->equals('demo');

        expect_not(Users::findIdentity(999));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = Users::findByUsername('demo'));
        expect($user->username)->equals('demo');

        expect_not(Users::findByUsername('not-admin'));
    }

    public function testFindUserByEmail()
    {
        expect_that($user = Users::findByEmail('demo@demo.hu'));
        expect($user->username)->equals('demo');

        expect_not(Users::findByEmail('not-admin@demo.com'));
    }

    public function testUserPass()
    {
        expect_that($user = Users::findByEmail('demo@demo.hu'));
        expect(Users::validatePassword('demo', $user->password))->equals(true);
    }

    public function testFindOne()
    {
        expect_that($user = Users::findone(['email' => 'demo@demo.hu']));
        expect($user->username)->equals('demo');

        expect_not($user = Users::findone(['email' => 'not-admin@demo.com']));
    }
}
