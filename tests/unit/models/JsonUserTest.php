<?php
namespace tests\unit\models;

use app\domains\ar\Users;
use app\domains\JsonUser;


class JsonUserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = JsonUser::findIdentity(1));
        expect($user->username)->equals('demo');

        expect_not(JsonUser::findIdentity(999));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = JsonUser::findByUsername('demo'));
        expect($user->username)->equals('demo');

        expect_not(JsonUser::findByUsername('not-admin'));
    }

    public function testFindUserByEmail()
    {
        expect_that($user = JsonUser::findByEmail('demo@demo.hu'));
        expect($user->username)->equals('demo');

        expect_not(JsonUser::findByEmail('not-admin@demo.com'));
    }

    public function testUserPass()
    {
        expect_that($user = JsonUser::findByEmail('demo@demo.hu'));
        expect(Users::validatePassword('demo', $user->password))->equals(true);
    }

    public function testFindOne()
    {
        expect_that($user = JsonUser::findone(['email' => 'demo@demo.hu']));
        expect($user->username)->equals('demo');

        expect_not($user = JsonUser::findone(['email' => 'not-admin@demo.com']));
    }
}
