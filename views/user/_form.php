<?php

use app\domains\ProfileForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\domains\ar\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?php
        if(!$model instanceof ProfileForm)
        {
            echo $form->field($model, 'email')->textInput();
        }
    ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'birthday',
        'value' => date('Y-m-d', strtotime('-20 year')),
        'options' => ['placeholder' => 'Select birthday ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
