<?php

namespace app\controllers;

use app\domains\ProfileForm;
use app\domains\RegisterForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\domains\LoginForm;

class UserController extends Controller
{
    public $defaultAction =  'login';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'profile'],
                'rules' => [
                    [
                        'actions' => ['logout', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
 * Login action.
 *
 * @return Response|string
 */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()  && $model->login()) {
            Yii::$app->session->setFlash('success', "Sikeres belépés");
            return $this->goHome();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Register action.
     *
     * @return Response|string
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate() && $model->register()) {
            Yii::$app->session->setFlash('success', "Sikeres regisztráció");
            return $this->goBack();
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Profile action.
     *
     * @return Response|string
     */
    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ProfileForm([
            'userid' => Yii::$app->user->id
        ]);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate() && $model->modify()) {
            Yii::$app->session->setFlash('success', "Sikeres profil módosítás");
            return $this->goBack();
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
