<?php

use app\domains\JsonDbHandler;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200426_143138_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $demoRow = [
            'username' => 'demo',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('demo'),
            'email' => 'demo@demo.hu',
            'birthday' => '2011-10-10',
        ];

        $this->createTable('{{%users}}', [
            'idusers' => $this->primaryKey(),
            'password' => $this->string(64),
            'username' => $this->string(64),
            'email' => $this->string(64)->notNull()->unique(),
            'birthday' => $this->date(),
        ]);

        $this->insert('{{%users}}', $demoRow);

        $jsonDb = new JsonDbHandler();
        $userRow = array_merge($demoRow, ['idusers' => 1]);
        $dbFile = [];
        array_push($dbFile, $userRow);

        $jsonDb::saveFile($dbFile);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
        $jsonDb = new JsonDbHandler();
        $dbFile = [];
        $jsonDb::saveFile($dbFile);
    }
}
